import React, { useState } from 'react'
import SearchIcon from '@material-ui/icons/Search';
import MicIcon from '@material-ui/icons/Mic';
import './Search.css'
import { Button } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import {useStateValue} from '../../StateProvider'
import {actionTypes} from '../../reducer'

const  Search = ({hideButtons=false}) => {
const [{},dispatch] = useStateValue();

const [input, setInput] = useState('');
const history = useHistory();

    const handleSearch = (e) => {
        e.preventDefault();

        console.log(input)
        console.log("you hit the search Button");
        dispatch({
            type:actionTypes.SET_SEARCH_TERM,
            term:input
        })
        history.push('/search');

        
    }

    return (
        <form className="search">
            <div className="search__input">
                <SearchIcon className="search__icon" />
                <input value={input} onChange={e=>setInput(e.target.value)}  type="search" />
                <MicIcon/>
            </div>
           {!hideButtons ? (
                <div className="search__buttons">
                <Button 
                type="submit"
                onClick={handleSearch} 
                variant="outlined">Google Search</Button>
                <Button variant="outlined">I'm Feeling lucky</Button>
                
            </div>
           ):(
            <div className="search__buttons">
            <Button 
            className="search__buttonsHidden"
            type="submit"
            onClick={handleSearch} 
            variant="outlined">Google Search</Button>
            <Button 
            className="search__buttonsHidden"
            variant="outlined"
            >I'm Feeling lucky</Button>
            
        </div>
           )}
        </form>
    )
}

export default Search
